<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 29.01.14
 * Time: 12:35
 */

namespace includes;

class Routing {

    public  $request;
    private $controller;
    private $action;
    private $controllerClass;
    private $controllerAction;


    public function __construct(){
        $this->request = array_filter(explode('/', $_SERVER['REQUEST_URI']));
        $this->route();

    }

    public function route(){
        $this->controller = isset($this->request[1]) ? $this->request[1] : "listing";
        $this->action     = isset($this->request[2]) ? $this->request[2] . "Action" : null;

        // Get request parameters
        $request_params = array();
        if(isset($this->request[3])) {
            // Remove first three elements from request array
            unset(
                $this->request[0],
                $this->request[1],
                $this->request[2]
            );

            // Set array with parameters
            $request_params = array_values($this->request);
        }


        if($this->checkController()){
            $controller = $this->controllerClass;
            $action     = $this->controllerAction;
            $controller->$action($request_params);
        }
    }

    // Checks if controller with such name exists
    public function checkController(){
        $this->controller;

        if(is_file(__DIR__.'/../controllers/' .ucfirst($this->controller). '.php')){
           require_once(__DIR__.'/../controllers/' .ucfirst($this->controller). '.php');
           $className = '\\controllers\\'.ucfirst($this->controller);
           $this->controllerClass = new $className();
           return $this->checkAction();
        }
        throw new \Exception(ucfirst($this->controller)." Controller not found");
    }

    // Checks if such action exists
    public function checkAction(){
        $this->controllerAction = $this->action;

        // Checking if the specified action exists within the controller
        if(method_exists($this->controllerClass, $this->controllerAction)){
            return true;
        }

        //  In the action is not specified in the URL checks if the index action is available
        if(method_exists($this->controllerClass, "indexAction")){
            $this->controllerAction = "indexAction";
            return true;
        }

        throw new \Exception($methodName." Action not found");
    }

}