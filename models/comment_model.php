<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 07.02.14
 * Time: 12:11
 */

class commentModel {

    public function __construct($db){
        $this->db = $db;
    }

    /**
     * Comment created with provided info
     *
     * @param $listing_id
     * @param $user_id
     * @param $comment_heading
     * @param $comment_text
     *
     * @return array with last comment info
     */
    public function createComment($listing_id,$user_id,$username,$comment_heading,$comment_text,$comment_date_time){

        $sth = $this->db->prepare("INSERT
                                   INTO   comments
                                   (`listing_id`,`user_id`,`username`,`comment_heading`, `comment_text`, `comment_date_time`)
                                   VALUES (:listing_id,:user_id,:username,:comment_heading,:comment_text,:comment_date_time)");

        $sth->execute(array(':listing_id' => $listing_id,
                            ':user_id' => $user_id,
                            ':username' => $username,
                            ':comment_heading' => $comment_heading,
                            ':comment_text' => $comment_text,
                            ':comment_date_time' => $comment_date_time));

        // return the id of the new created listing
        return $this->db->lastInsertId();
    }

    /**
     * Extracts all comments associated with the listing
     *
     * @param $listing_id
     *
     * @return associative array with all comments related to the listing
     */
    public function getComments($start, $listing_id){


        $q = "SELECT *
            FROM comments
            WHERE listing_id = :listing_id
            ORDER BY comment_date_time DESC
            LIMIT $start," . PER_PAGE;


        $sth = $this->db->prepare($q);
        $sth->execute(array(':listing_id' => $listing_id));
        return $sth -> fetchall(PDO::FETCH_ASSOC);
    }

    /**
     * Deletes comment with the $comment_id
     *
     * @param $comment_id
     */
    public function deleteComment($comment_id){
        $q="DELETE
            FROM comments
            WHERE comment_id = :comment_id";
        $sth = $this->db->prepare($q);
        $sth->execute(array(':comment_id' => $comment_id));
    }

    public function totalComments($listing_id){
        $q="SELECT count(*)
            AS comments_count
            FROM comments
            WHERE listing_id = :listing_id";

        // Executing the query
        $sth = $this->db->prepare($q);
        $sth->execute(array(':listing_id' => $listing_id));

        // Returning the number of the listings
        return $sth -> fetch(PDO::FETCH_ASSOC);

    }

} 