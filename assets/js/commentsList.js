$(document).ready(function(){

    var showComments = function(cp){
        $('.comments').remove();
        $.ajax({
            type: 'POST',
            url: 'http://www.listings.local/listing/showComments',
            data: {
                listing_id:$('.LI').attr('value'),
                current_page: (cp-1) || 0
            },
            dataType: 'html',
            success: function(data){
                $('.allComments').after(data);
            }
        });
    };

    showComments();


    $('#addComment').submit(function(event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: $(this).attr("action"),
            data: $(this).serialize(),
            dataType: 'html',
            success: function(){
                $('#commentHeading').val(''),
                $('#commentText').val('')
            }
        });
        showComments();
    });

    $(document).on('click', '#deleteComment', function(event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: $(this).attr("href"),
            data: {
                comment_id : $(this).attr("commentId"),
                listing_id : $(this).attr("listingId")
            },
            dataType: 'html'
        });
        showComments();
    });

    $(document).on('click', '#pagination', function(event) {
        //attr('value')
        showComments($(this).attr('page_num'));
    })
})