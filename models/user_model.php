<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 31.01.14
 * Time: 13:17
 */

/**
 * Class UserModel
 *
 * @author Kanat <kanat@gmail.com>
 */
class UserModel{

    public function __construct($db){
        $this->db = $db;
    }


    /**
     * The function is used for login purposes. It checks if the login and password belong to a single user and logs the person if so.
     * @param $username
     * @param $password
     *
     * @return bool
     */
    public function getUserByUsernameAndPassword($username,$password){

        // Select user by username and password
        $sth = $this->db->prepare("SELECT *
                                   FROM   users
                                   WHERE  username = :username
                                          AND    password = :password");
        $sth->execute(array(':username' => $username, ':password' => $password));
        $count =  $sth->rowCount();
        $result = $sth->fetch();
        // END

        // if there's NOT one result (0 or more then one, which means that there may be instances of using such password, but not by the user with such username.)
        if ($count != 1) {
            return false;
        }
        return $result;

    }

    /**
     * Creates user using with the provided information.
     * @param string $username username
     * @param string $password password of the user
     * @param string $cell cellphone number
     * @return username for starting session purposes
     */
    public function createUser($username,$password,$cell){

        $sth = $this->db->prepare("INSERT
                                   INTO   users
                                   (`username`,`password`,`cell`) VALUES (:username,:password,:cell)");
        $sth->execute(array(':username' => $username, ':password' => $password, ':cell' => $cell));

        return $this->getUserByUsername($username);

    }

    /**
     * Function gets the username for various purposes (for example checking if username is already taken)
     * @param string $username username
     * @return object with users information
     */
    public function getUserByUsername($username){
        $sth = $this->db->prepare("SELECT *
                                   FROM   users
                                   WHERE  username = :username");
        $sth->execute(array(':username' => $username));
        return  $sth->fetch(PDO::FETCH_OBJ);

    }

}