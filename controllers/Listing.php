<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 30.01.14
 * Time: 16:45
 */

namespace controllers;

use includes\Basecontroller;

class Listing extends Basecontroller{

    /**
     * shows the 'Add Listing' page and is also responsible for processing listing adding
     *
     * @param array $request_params
     */
    public function addAction($request_params = array()){

        // Action - post listing
        if($_SERVER["REQUEST_METHOD"]=="POST"){

            // The if/else checks if the necessary information has been filled
            if (empty($_POST['heading']) OR empty($_POST['description'])){
                echo "<p class='warning'>Please don't forget to fill heading and description</p> <hr />";
            }else{

                // Gets category id using the category title
                $category_model = $this->loadModel('Category');
                $category_title = $_POST['category_title'];
                $category_id = $category_model -> getCategoryId($category_title);



                // Listing model and image model loader, and all the information proceeded to create a listing
                $listing_model = $this->loadModel('Listing');
                $image_model = $this->loadModel('Image');
                $user_id = $_SESSION['userid'];
                $heading = $_POST['heading'];
                $description = $_POST['description'];
                $price = $_POST['price'];

                // Listing is create and listing id is stored to a variable
                $listing_id = $listing_model->createListing($user_id,$heading,$description,$category_id,$price);

                // The code below determines the directory for uploading images
                $listing_image_dir = $this->getImageDir($user_id,$listing_id);

                // Arrays are reorganized for mor convenient handling and processing
                $FileArray = $this->reArrayFiles($_FILES['file']);

                // Files are saved (in case they wre sent) in the listing directory of the user directory in images directory and corresponding record is saved to the Database
                foreach ($FileArray as $file) {
                    $upload_file = $listing_image_dir . time() .  basename($file['name']);
                    $image_name = time() . basename($file['name']);
                    $image_success = move_uploaded_file($file['tmp_name'], $upload_file);
                    if($image_success){
                        $image_model -> createImage ($image_name, $listing_id);
                    }

                }


                // After the creation process the user is headed to the listing information page
                header('location: ' . BASEURL . '/listing/show/'.$listing_id);

            }
        }

        // "Add listing page" for logged users. Sending non-logged users to the main page
        if (!$this->loggedin()){
            header('location: ' . BASEURL . '/listing');
        }else{
            $category_model = $this->loadModel('Category');
            $category_list = $category_model-> getCategoryIdAndNames();
            $data['templates'][1] = 'footer';
            $this->render('add', $category_list);
        }
    }

    /**
     * Shows the listing info
     *
     * @param array $request_params the listing_id is extracted from the url
     */
    public function showAction($request_params = array()){

        // The listing id is extracted from the URL
        $listing_id = $request_params[0];

        // Listing info is got from the database
        $listing_model = $this->loadModel('Listing');
        $data = $listing_model->getListingInfo($listing_id);

        // Image info is got from the database on the bases of the listing id
        $image_model = $this->loadModel('Image');
        $image_info = $image_model->getImageByListingId($listing_id);
        if($image_info){
            $data['imageid'] = $image_info;
        }


        //$data['comments'] = $this-> getComments($listing_id);
        //$data['add_content'] = 'commentsList';
        $this->render('listinfo', $data);

    }

    /**
     * Displaying listings and categories on the main page
     *
     * @param array $request_params holds the number of the displayed page
     */
    public function indexAction($request_params = array()){

        // Page number extracted from the URL. If empty then it is set to 0.
        if(!empty($request_params[0])){
            $current_page = $request_params[0]-1;
        } else {
            $current_page = 0;
        }

        // Categories information is got to display on the main page.
        $category_model = $this->loadModel('category');
        $category_names = $category_model-> getCategoryIdAndNames();

        // Listing information is got on the basis. Conditions are empty for the main page.
        $data = $this->calcPagesDisplayListings(null, null, $current_page);
        $data = array_merge($data,array('categories' => $category_names));
        $this->render('list', $data);
    }

    /**
     *Displays the user's listings
     *
     * @param array $request_params the number of the page of listings is extracted from that parameter
     */
    public function yourListingsAction($request_params = array()){

        // Page number extracted from the URL. If empty then it is set to 0.
        if(!empty($request_params[0])){
            $current_page = $request_params[0]-1;
        } else {
            $current_page = 0;
        }

        // User_id is extracted from the session
        $user_id = $_SESSION['userid'];
        $data = $this->calcPagesDisplayListings($user_id, null, $current_page);

        //the page is rendered with the data provided
        $this->render('yourListings', $data);
    }

    /**
     * Displays listings by category
     *
     * @param array $request_params
     */
    public function categoryListingsAction($request_params = array()){

        // Category_id is extracted from the url
        $category_id = $request_params[0];

        // Page number extracted from the URL. If empty then it is set to 0.
        if(!empty($request_params[1])){
            $current_page = $request_params[1]-1;
        } else {
            $current_page = 0;
        }
        $data = $this->calcPagesDisplayListings(null, $category_id, $current_page);
        $data = array_merge($data,array('categoryId' => $request_params[0]));
        $this->render('categoryListings', $data);
    }

    /**
     * @param array $request_params
     */
    public function searchAction($request_params = array()){

        if($_SERVER["REQUEST_METHOD"]=="POST"){
            if (empty($_POST['search'])){
                header('location: ' . BASEURL . '/listing');
            }

            $current_page = 0;
            $search_cond = $_POST['search'];

        }else{

            $search_cond = $request_params[0];
            if(!empty($request_params[1])){
                $current_page = $request_params[1]-1;
            } else {
                $current_page = 0;
            }

        }

        $data = $this->searchData($current_page,$search_cond);
        $this->render('search', $data);

    }

    /**
     * The function deletes the listing according to its' id and heads back to the user page
     *
     * @param array $request_params holds the number of the listing_id that should be deleted
     */
    public function deleteAction($request_params = array()){

        $listing_id = $request_params[0];
        $listing_model = $this->loadModel('Listing');
        $listing_owner = $listing_model->getListingInfo($listing_id)['userid'];
        if(!($listing_owner == $_SESSION['userid'])){
            header('location: ' . BASEURL . '/listing');
        }else{$listing_model->deleteListing($listing_id);
        }

        header('location: ' . BASEURL . '/listing/yourListings');
    }

    /**
     *  Function for displaying 'edit listing' page and editing the data inside.
     *
     *  @param array $request_params
     */
    public function editAction($request_params = array()){

        //  Checks if the info was posted or just requested
        if($_SERVER["REQUEST_METHOD"]=="POST"){

            // Checks if the necessary information is filled
            if (empty($_POST['heading']) OR empty($_POST['description'])){
                echo "<p class='warning'>Please don't forget to fill heading and description</p> <hr />";
            }else{

                // Listing model loaded, and all the information proceeded to create a listing
                // gets category id first
                $category_model = $this->loadModel('Category');
                $category_title = $_POST['category_title'];
                $category_id = $category_model -> getCategoryId($category_title);

                // Gets the rest of the data
                $listing_model = $this->loadModel('Listing');
                $listing_id = $_POST['listingId'];
                $heading = $_POST['heading'];
                $description = $_POST['description'];
                $price = $_POST['price'];

                //  Listing info edited
                $listing_model->editListing($listing_id,$heading,$description,$category_id,$price);

                // Image editing is processed
                $image_model = $this->loadModel('Image');
                $user_id = $_SESSION['userid'];

                // The code below determines the directory for uploading images
                $listing_image_dir = $this->getImageDir($user_id,$listing_id);

                // Image arrays reorganized for more convenient handling
                $FileArray = $this->reArrayFiles($_FILES['file']);

                // Images saved to the images/user_id/listing_id folder and image information is stored in the database
                foreach ($FileArray as $file) {
                    $upload_file = $listing_image_dir . time() .  basename($file['name']);
                    $image_name = time() . basename($file['name']);
                    $image_success = move_uploaded_file($file['tmp_name'], $upload_file);
                    if($image_success){
                        $image_model -> createImage ($image_name, $listing_id);
                    }

                }

                //make a page to head to the list info
                header('location: ' . BASEURL . '/listing/show/'.$listing_id);

            }
        }




        //  Listing id extracted from the URL
        $listing_id = $request_params[0];
        $listing_model  = $this->loadModel('Listing');

        $data = $listing_model->getListingInfo($listing_id);

        // checks if the editor is the owner of the listing
        $listing_owner = $data['userid'];
        if(!($listing_owner == $_SESSION['userid'])){
            header('location: ' . BASEURL . '/listing');}

        // loading all images and their info for the listing
        $image_model = $this->loadModel('Image');
        $image_info = $image_model->getImageByListingId($listing_id);
        if($image_info){
            $data['imageid'] = $image_info;
        }

        // loading list of category names to choose
        $category_model = $this->loadModel('Category');
        $category_list = $category_model-> getCategoryIdAndNames();

        $data['category_list'] = $category_list;

        //  Edit page is rendered with the listing information.
        $this->render('edit', $data);
    }



    /**
     * The function calculates the required number of pages to display a cetain number of listings
     *
     * @param int $listings_count - total number of listings to display
     * @param int PER_PAGE - defined in configuration - number of listings displayed per page
     * @return int - total number of pages to display
     */
    private function getPagesCount($listings_count) {
        return ceil($listings_count['listings_count']/PER_PAGE);
    }

    /**
     *  Returns listings info and calculates the number of pages for displaying all the listings
     *
     * @param string    $user_id
     * @param string    $category       category title
     * @param string    $currentPage    number of the current page
     *
     * @return array
     */
    private function calcPagesDisplayListings($user_id, $category, $currentPage){

        // The model is loaded and total number of listings is counted
        $listing_model  = $this->loadModel('Listing');
        $listings_count = $listing_model->totalListings($user_id,$category);

        // Total number of pages
        $count_of_pages = $this->getPagesCount($listings_count);

        // The number of listing from which the page should start displaying all the listings
        $start = $currentPage*PER_PAGE;

        //the associative array is returned with the listings of the user that will be displayed on the page
        $page_listings  = $listing_model->pageListings($start,$user_id, $category);

        $data = array(
            "current_page"   => $currentPage,
            "count_of_pages" => $count_of_pages,
            "listings"       => $page_listings
        );

        return $data;
    }

    /**
     * The function creates a new folder with user_id name and subfolder with listing_id name in case that they don't exist
     *
     * @param integer   $user_id
     * @param integer   $listing_id
     * @return string   the directory name
     */
    private function getImageDir($user_id,$listing_id){
        $upload_dir = $_SERVER['DOCUMENT_ROOT']. 'images/';
        if(!file_exists($upload_dir.$user_id. '/')){
            mkdir($upload_dir . $user_id, 0777);
        }
        $user_dir = $upload_dir . $user_id . '/';
        if(!file_exists($user_dir.$listing_id. '/')){
            mkdir($user_dir . $listing_id, 0777);
        }
        return $user_dir . $listing_id . '/';
    }

    /**
     *  Function reorganizes file information into more convenient structure
     *
     * @param $file_post
     * @return array
     */
    function reArrayFiles(&$file_post) {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }

    /**
     *  Deletes image with the provided information
     *
     *  User_id and Listing_id are used to find the folder of the image.
     */
    public function deleteImageAction(){
        $image_model  = $this->loadModel('image');
        $user_id = $_SESSION['userid'];
        $listing_id = $_POST['listingId'];
        $image_id = $_POST['imageId'];
        $listing_image_dir = $this->getImageDir($user_id,$listing_id);
        $image_model -> deleteImage($image_id,$listing_image_dir);
        header('location: ' . BASEURL . '/listing/edit/'.$listing_id);
    }

    /**
     *  Returns the data of the listings accoring to search conditions
     *
     * @param $current_page
     * @param $search_cond
     * @return array
     */
    public function searchData($current_page, $search_cond){

        // The model is loaded and total number of listings is counted
        $listing_model  = $this->loadModel('Listing');
        $listings_count = $listing_model->totalSearchListings($search_cond);

        // Total number of pages
        $count_of_pages = $this->getPagesCount($listings_count);

        // The number of listing from which the page should start displaying all the listings
        $start = $current_page*PER_PAGE;

        //the associative array is returned with the listings of the user that will be displayed on the page
        $page_listings  = $listing_model->pageSearchListings($start,$search_cond);


        $data = array(
            "current_page"   => $current_page,
            "count_of_pages" => $count_of_pages,
            "listings"       => $page_listings,
            "search_cond"    => $search_cond
        );

        return $data;

    }

    /**
     *  Add comment function
     */
    public function addCommentAction(){

        if($_SERVER["REQUEST_METHOD"]=="POST"){
            // Checks if the necessary information is filled
            if (empty($_POST['comment_text'])){
                echo "<p class='warning'>Please don't forget to fill heading and description</p> <hr />";
            }else{

                $comment_model = $this->loadmodel('comment');
                $listing_id = $_POST['listing_id'];
                $user_id = $_SESSION['userid'];
                $username = $_SESSION['username'];
                $comment_heading = $_POST['comment_heading'];
                $comment_text = $_POST['comment_text'];
                $comment_date_time = date('Y-m-d H:i:s');

                $comment_model->createComment($listing_id,$user_id,$username,$comment_heading,$comment_text,$comment_date_time);

            }
        }
    }

    /**
     *  Deletes comment
     */
    public function deleteCommentAction() {

        if($_SERVER["REQUEST_METHOD"]=="POST"){
            $comment_model = $this->loadmodel('comment');
            $comment_id = $_POST['comment_id'];
            $comment_model->deleteComment($comment_id);
        }

    }

    /**
     *  Gets all the comment information related to the listing with $listing_id
     *
     * @param   string  $listing_id
     * @return  array
     */
    public function getComments($listing_id){
        $comment_model = $this->loadmodel('comment');
        $comment_data = $comment_model -> getComments($listing_id);
        if($comment_data){
            rsort($comment_data);
            return $comment_data;
        }
        return array();
    }

    /**
     *  Shows comments
     *
     */
    public function showCommentsAction(){

        $listing_id = $_POST['listing_id'];
        $current_page = $_POST['current_page'];

        $current_page = intval($current_page);

        $comment_model  = $this->loadModel('Comment');
        $comments_count = $comment_model->totalComments($listing_id);

        $count_of_pages = ceil($comments_count['comments_count']/PER_PAGE);

        $start = $current_page * PER_PAGE;

        $page_comments  = $comment_model->getComments($start,$listing_id);

        $data['current_page'] = $current_page;
        $data['comments'] = $page_comments;
        $data['count_of_pages'] = $count_of_pages;
        return $this->renderPartial('commentsList', $data);

    }

} 