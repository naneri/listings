<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 29.01.14
 * Time: 13:37
 */

namespace includes;


class Basecontroller {

    function __construct()
    {
        Session::init();


        // create database connection
        try {
            $this->db = new Database();
        } catch (PDOException $e) {
            die('Database connection could not be established.');
        }

    }

    /**
     * Checks if user is logged in
     *
     * @return bool
     */
    public function loggedin(){
        if (!empty($_SESSION['userid'])){
            return true;
        }
    }

    /**
     * Renders the page using the template and inserting all the data
     *
     * @param $template
     * @param array $data
     *
     * returns nothing
     */
    public function render($template, $data = array()){
        require 'connect.php';
        require 'view/start.php';



        // buffering the page to display it with the provided info
        ob_start();

        // Different menu is displayed for logged and non-logged users
        if ($this->loggedin()){

            $this->renderMenu('auth');
        }else{$this->renderMenu('noauth');}

        require 'view/'.$template.'.php';
        echo ob_get_clean();



        //  Additional templates may be attached to $data['add_content']
   /*     if(array_key_exists('add_content', $data)){
            $this->renderPartial($data['add_content'],$data);
        }
   */


        require 'view/footer.php';
    }

    public function renderPartial($template, $data = array()){

        require 'connect.php';

        ob_start();
        require 'view/'.$template.'.php';
        echo ob_get_clean();

    }

    /**
     * rendering menu
     *
     * @param $template
     */
    public function renderMenu($template){
        require 'view/'.$template.'.php';
    }

    /**
     *  Model is loaded acording to requested parametr
     *
     * @param $name
     * @return mixed
     */
    public function loadModel($name)
    {
        // Defining the path for the models
        $path = 'models/' . strtolower($name) . '_model.php';
        if (file_exists($path)) {
            require_once 'models/' . strtolower($name) . '_model.php';
            // The "Model" has a capital letter as this is the second part of the model class name,
            // all models have names like "imageModel"
            $modelName = $name . 'Model';
            // return the new model object while passing the database connection to the model
            return new $modelName($this->db);
        }
    }

} 