<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 03.02.14
 * Time: 11:17
 */

class imageModel {

    public function __construct($db){
        $this->db = $db;
    }

    /**
     * The function stores all the information concerning the image in the Database
     *
     * @param   string    $image_name the name of the image in the directory
     * @param   integer   $listing_id id of the listing to which the image will be connected
     *
     * returns nothing
     */
    public function createImage($image_name,$listing_id){

        //inserts the new image in the DB
        $sth = $this->db->prepare("INSERT
                                   INTO   images
                                   (`image_name`,`listingid`) VALUES (:image_name,:listingid)");
        $sth->execute(array(':image_name' => $image_name, ':listingid' => $listing_id));

    }

    /**
     * Gets all the Image info of the listing with listing_id to an associative array
     *
     * @param   integer $listing_id the id of the listing to which image is connected
     *
     * @return  array   The array contains all the image info
     */
    public function getImageByListingId($listing_id){
        $sth = $this->db->prepare("SELECT *
                                   FROM   images
                                   WHERE  listingid = :listing_id");
        $sth->execute(array(':listing_id' => $listing_id));
        return  $sth->fetchAll();
    }

    /**
     * Deletes image from database and folder with ID of the image and directory of listing's images provided
     *
     * @param string $image_id              id of the image
     * @param string $listing_image_dir     path to the images of the listing
     *
     * returns nothing
     */
    public function deleteImage($image_id, $listing_image_dir){
        $sth = $this->db->prepare("SELECT *
                                   FROM   images
                                   WHERE  imageid = :image_id");
        $sth->execute(array(':image_id' => $image_id));
        $image_name = $sth->fetchColumn(1);

        $sth = $this->db->prepare("DELETE
                                   FROM   images
                                   WHERE  imageid = :image_id");
        $sth->execute(array(':image_id' => $image_id));
        unlink($listing_image_dir . $image_name);

    }
}

