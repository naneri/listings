

<h1>Edit listing:</h1>

<?php if(array_key_exists('imageid',$data)) : ?>
    <?php foreach($data['imageid'] as $image) : ?>
        <img src='<?php echo '/images/' . $data['userid'] . '/' . $data['listingid'] . '/' . $image->image_name;?>'>

        <form action="<?php echo BASEURL; ?>/listing/deleteImage" method="post">
            <input type="hidden" name="imageId" value="<?php echo $image->imageid; ?>">
            <input type="hidden" name="listingId" value="<?php echo $image->listingid; ?>">

            <input class="form-control" type="submit" name="edit-submit" value="Delete"/>
        </form>
        <br /><br />
    <?php endforeach; ?>
<?php endif; ?>

<form  action="<?php echo BASEURL; ?>/listing/edit" method="post" enctype="multipart/form-data">
    <p class="text-center">Listing Header:</p>
    <input class="form-control" type="text" name="heading" value="<?php echo $data['heading']?>"/> <br />





    <p class="text-center">Description:</p>
    <textarea class="form-control" rows="5" name="description"><?php echo $data['description']?></textarea><br/>
    <p class="text-center">Category:</p>
    <select name="categorytitle">

        <?php foreach($data['category_list'] as $key=>$value):?>
            <option value='<?php echo $value ?>' <?php if($value == $data['categorytitle']){echo 'selected';}?>><?php       echo $value ?></option>
        <?php endforeach; ?>

    </select><br/>
    <p class="text-center">Price:</p>
    <input class="form-control" type="text" name="price" value="<?php echo $data['price']?>"/><br/><br/>
    <p class="text-center">Add image:</p>
    <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
    <input type="file" name="file[]" accept="image/*" multiple><br>
    <input type="hidden" name="listingId" value="<?php echo $data['listingid']; ?>">

    <input class="form-control" type="submit" name="edit-submit" value="Edit"/>


</form>