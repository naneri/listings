

<h1>Add listing:</h1>

<form  action="<?php echo BASEURL; ?>/listing/add" method="post" enctype="multipart/form-data">
    <p class="text-center">Listing Header:</p>
    <input class="form-control" type="text" name="heading"/>
    <p class="text-center">Description:</p>
    <textarea class="form-control" rows="5" name="description"></textarea><br/>
    <p class="text-center">Category:</p>
    <select name="category_title">

        <?php foreach($data as $key=>$value):?>

            <option value='<?php echo $value; ?>'> <?php echo $value; ?></option>
        <?php endforeach;?>

    </select><br/>
    <p class="text-center">Price:</p>
    <input class="form-control" type="text" name="price"/><br/><br/>
    <p class="text-center">Add image:</p>
    <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
    <input type="file" name="file[]" accept="image/*" multiple><br>

    <input class="form-control" type="submit" name="add-submit" value="Add"/>


</form>