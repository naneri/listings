<?php
	//please insert the parameters ("database location", "login", "password", "database name")
	$con=mysqli_connect("localhost","root","","list");

	// Checking connection
	if (mysqli_connect_errno()){
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

	$sql1 ="CREATE TABLE users (
	    userid INT NOT NULL AUTO_INCREMENT,
	    PRIMARY KEY(userid),
	    username VARCHAR(255),
	    password VARCHAR(255),
        cell VARCHAR(25)
	    )";

    $sql2 ="CREATE TABLE listings (
	    listingid INT NOT NULL AUTO_INCREMENT,
	    PRIMARY KEY(listingid),
	    userid INT(15),
	    heading Text(255),
	    description Text(65535),
        categoryid Int,
        price FLOAT(15)
	    )";

    $sql3 ="CREATE TABLE images (
	    imageid INT NOT NULL AUTO_INCREMENT,
	    PRIMARY KEY(imageid),
        image_name VARCHAR(255),
        listingid INT
	    )";

    $sql4 ="CREATE TABLE categories (
	    categoryid INT NOT NULL AUTO_INCREMENT,
	    PRIMARY KEY(categoryid),
	    categorytitle VARCHAR(255),
	    slug TEXT
	    )";




	if (mysqli_query($con,$sql1))
    {
        echo "Table created successfully";
    }
    else
    {
        echo "Error creating table: " . mysqli_error($con);
    }


	if (mysqli_query($con,$sql2))
    {
        echo "Table created successfully";
    }
    else
    {
        echo "Error creating table: " . mysqli_error($con);
    }


	if (mysqli_query($con,$sql3))
    {
        echo "Table created successfully";
    }
    else
    {
        echo "Error creating table: " . mysqli_error($con);
    }


	if (mysqli_query($con,$sql4))
    {
        echo "Table created successfully";
    }
    else
    {
        echo "Error creating table: " . mysqli_error($con);
    }
?>

