<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 29.01.14
 * Time: 12:28
 */

namespace controllers;

use includes\Basecontroller;
use includes\Session;

class user extends Basecontroller {

        // The index action is login user - it takes the login and password on the login page, checks the information in Database and logs the user if everything is correct.
    public function indexAction(){
        if($_SERVER["REQUEST_METHOD"]=="POST"){

            // Checks if the necessary info is filled
            if (empty($_POST['username']) OR empty($_POST['password'])){
                echo "<p>Please fill all the fields</p> <hr />";

            }else{
            $user_model = $this->loadModel('User');
            $username = $_POST['username'];
            $password = $_POST['password'];
            $user= $user_model->getUserByUsernameAndPassword($username,$password);
            if($user){
                Session::set('user_logged_in', true);
                Session::set('userid', $user->userid);
                Session::set('username', $user->username);
                header('location: ' . BASEURL);
            }
                echo "<p>Username or password is wrong</p> <hr />";
            }

        }

       $this->render('login');
    }

        //the logout function
    public function logoutAction(){
        Session::destroy();
        header('location: ' . BASEURL . '/listing');
    }

    /**
     * the register function leads to the register page and uses all the provided information to register a new user.
     */
    public function registerAction(){

        // Registering a new user
        if($_SERVER["REQUEST_METHOD"]=="POST"){

            // Showing mistake not all necessary info is filled
            if (empty($_POST['username']) OR empty($_POST['password'])){
                echo "<p>Please fill Username and Password</p>";
                echo "<a href='../Register'>Go back</a>";

            }else{

                // Processing registration
                $user_model = $this->loadModel('User');
                $username = $_POST['username'];
                $password = $_POST['password'];
                $cell = $_POST['cell'];
                $user = $user_model->getUserByUsername($username);
                if ($user){
                    echo "<p>Username have been already taken</p>";
                    echo "<a href='../Register'>Go back</a>";
                }else{
                    $user =  $user_model->createUser($username,$password,$cell);
                    Session::set('user_logged_in', true);
                    Session::set('userid', $user->userid);
                    header('location: ' . BASEURL);

                }

            }

        }

        // Showing registration page if method is not post
        $this->render('register');
    }



}