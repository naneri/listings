<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 03.02.14
 * Time: 11:17
 */

class categoryModel {

    public function __construct($db){
        $this->db = $db;
    }


    /**
     * Without any inputs simply returns the array with category names
     *
     * @return array of the category names and ids
     */
    public function getCategoryIdAndNames(){

        $sth = $this->db->prepare("SELECT *
                                   FROM   categories");
        $sth->execute();
        $category_ids = $sth->fetchAll(PDO::FETCH_COLUMN, 0);
        $sth->execute();
        $names =  $sth->fetchAll(PDO::FETCH_COLUMN, 1);
        $category_data = array_combine($category_ids,$names);
        return $category_data;
    }


    /**
     * Gets category id if category title is provided
     *
     * @param string $categorytitle
     * @return int - the id of the category (may be used in listing info or any other place)
     */
    public function getCategoryId($category_title){
        $sth = $this->db->prepare("SELECT categoryid
                                   FROM   categories
                                   WHERE categorytitle = :category_title");
        $sth->execute(array(':category_title' => $category_title));
        return $sth->fetchColumn();
    }

}