<div class="comments" page="<?php echo $data['comments_page'];?>">
    <?php if(array_key_exists('comments',$data)): ?>
        <?php foreach($data['comments'] as $comment):?>

            <div>
                <p>User:<?php echo $comment['username']?></p>
                <p>Date/Time:<?php echo $comment['comment_date_time']?></p>
                <h3>Topic:<?php echo $comment['comment_heading']?></h3>
                <p>Text:<?php echo $comment['comment_text']?></p>
    <?php if(!empty($_SESSION['userid'])): ?>
        <?php  if($comment['user_id'] == $_SESSION['userid']): ?>
                <a id="deleteComment"
                   commentId="<?php echo $comment['comment_id']?>"
                   listingId="<?php echo $comment['listing_id']?>"
                   href="<?php echo BASEURL; ?>/listing/deleteComment/">
                        Delete comment
                </a>
        <?php endif; ?>
    <?php endif; ?>

            </div>
            <hr />
        <?php endforeach;?>

        <div class = "pagination" style="text-align: center">
            <?php for($i=1; $i<=$data['count_of_pages']; $i++) : ?>
                <?php if(($i - 1) == $data['current_page']) : ?>
                    <?php echo $i . " "; ?>
                <?php else : ?>
                    <a href="#" id="pagination" page_num="<?php echo $i;?>"><?php echo $i; ?></a>
                <?php endif; ?>
            <?php endfor; ?>
        </div>

    <?php endif;?>
</div>