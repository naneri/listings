<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 03.02.14
 * Time: 11:21
 */

class listingModel {

    public function __construct($db){
        $this->db = $db;
    }


    /**
     * Create listing
     *
     * @param integer   $userid         ID of user
     * @param string    $heading        Heading
     * @param string    $description    Description
     * @param integer   $categoryid     ID of category
     * @param float     $price          Price
     *
     * Returns int - the listing id
     */
    public function createListing($user_id,$heading,$description,$category_id,$price){

        $sth = $this->db->prepare("INSERT
                                   INTO   listings
                                   (`userid`,`heading`,`description`, `categoryid`, `price`)
                                   VALUES (:user_id,:heading,:description,:category_id,:price)");
        $sth->execute(array(':user_id' => $user_id, ':heading' => $heading, ':description' => $description,                                      ':category_id' => $category_id, ':price' => $price ));

        // return the id of the new created listing
        return $this->db->lastInsertId();


    }

    /**
     * Editing the listing that has id equal to listing id with provided information
     *
     * @param string        $listing_id
     * @param string        $heading
     * @param string        $description
     * @param string        $category_id
     * @param integer       $price
     *
     * Returns nothing
     */
    public function editListing($listing_id,$heading,$description,$category_id,$price){

        $sth = $this->db->prepare("UPDATE listings
                                   SET    heading = :heading,
                                          description = :description,
                                          categoryid = :category_id,
                                          price = :price
                                   WHERE  listingid = :listing_id");
        $sth->execute(array(':listing_id' => $listing_id, ':heading' => $heading, ':description' => $description,                                      ':category_id' => $category_id, ':price' => $price ));


    }

    /**
     * The function gets the listinginfo to an array if listingid is provided
     * @param int $listingid the id of the listing
     *
     * @return associative array key=>value with the information of the listing
     */
    public function getListingInfo($listing_id){
        $sth = $this->db->prepare("SELECT *
                                   FROM   listings
                                   LEFT JOIN users
                                   ON listings.userid = users.userid
                                   LEFT JOIN categories
                                   ON listings.categoryid = categories.categoryid
                                   WHERE  listingid = :listing_id");
        $sth->execute(array(':listing_id' => $listing_id));
        return $sth -> fetch(PDO::FETCH_ASSOC);
    }

    /**
     * The function for calculating the total number of listings matching the conditions (all listings or user's listings or category listings).
     *
     * @param integer   $userid     [optional parameter] For calculating listings of certain user
     * @param integer   $category   [optional parameter] For calculating listings in certain category
     *
     * @return array     returns array with ['listings_count'] element holding the number of listings
     */
    public function totalListings($user_id = null, $category_id = null){
        // The SQL query
        $q="SELECT count(*)
            AS listings_count
            FROM listings";

        // The replacement array for the execute() function. Checks if the user_id or category is available and adds the clause in case it is available.
        $whereArray = array();
        if(!is_null($user_id)){
            $q .= " WHERE userid = :user_id";
            $whereArray = array(':user_id' => $user_id);
        }
        if(!is_null($category_id)){
            $q .= " WHERE categoryid = :category_id";
            $whereArray = array(':category_id' => $category_id);
        }

        // Executing the query
        $sth = $this->db->prepare($q);
        $sth->execute($whereArray);

        // Returning the number of the listings
        return $sth -> fetch(PDO::FETCH_ASSOC);

    }

    /**
     * The function returns all the listings for a single page to an array to display them later on the main page.
     *
     * @param int $pageNumber
     *
     * @return associative array with listings' information
     */
    public function pageListings($start, $user_id = null, $category_id = null){


        // Set where clause
        $whereArray = array();
        $where = "";
        if(!is_null($user_id)){
            $where .= " WHERE userid = :user_id";
            $whereArray = array(':user_id' => $user_id);
        }
        if(!is_null($category_id)){
            $where .= " WHERE categoryid = :category_id";
            $whereArray = array(':category_id' => $category_id);
        }

        //  Finding all the listings and returning the result
        $q="SELECT *
            FROM listings
            " . $where . "
            ORDER BY listingid
            LIMIT $start,". PER_PAGE;
        $sth = $this->db->prepare($q);
        $sth->execute($whereArray);
        return $sth -> fetchall(PDO::FETCH_ASSOC);
    }


    /**
     * The function deletes the listing by id
     *
     * @param int $listingid the id of the listing that you need to delete
     *
     * Returns nothing
     */
    public function deleteListing($listing_id){
        $q="DELETE
            FROM listings
            WHERE listingid = :listing_id";
        $sth = $this->db->prepare($q);
        $sth->execute(array(':listing_id' => $listing_id));
    }

    /**
     * Returns number of listings that match the search condition
     *
     * @param   string  $search_cond the search condition
     *
     * @return  mixed   associative array, with ['listings_count'] element holding the total number of matching listings
     */
    public function totalSearchListings($search_cond){
        // The SQL query
        $sth = $this->db->prepare("SELECT count(*)
            AS listings_count
            FROM listings
            WHERE heading LIKE :something
            OR description LIKE :something");
        $sth->execute(array(':something' => '%' . $search_cond . '%'));

        // Returning the number of the listings
        return $sth -> fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Listings for the page that math the search condition
     *
     * @param string    $page_number    the number of the page with matching listings
     * @param string    $search_cond    the search conditions
     *
     * @return mixed    the listings
     */
    public function pageSearchListings($page_number, $search_cond){
        // Set where clause
        $sth = $this->db->prepare("SELECT *
            FROM listings
            WHERE heading LIKE  :something
            OR description LIKE :something
            ORDER BY listingid
            LIMIT $page_number,". PER_PAGE);
        $sth->execute(array(':something' => '%' . $search_cond . '%'));
        return $sth -> fetchall(PDO::FETCH_ASSOC);
    }

} 